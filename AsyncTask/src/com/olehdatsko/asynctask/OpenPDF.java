package com.olehdatsko.asynctask;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

public class OpenPDF extends net.sf.andpdf.pdfviewer.PdfViewerActivity {

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.open_pd, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public int getNextPageImageResource() {
		return R.drawable.right_arrow;
	}

	@Override
	public int getPdfPageNumberEditField() {
		return R.id.pagenum_edit;
	}

	@Override
	public int getPdfPageNumberResource() {
		return R.layout.dialog_pagenumber;
	}

	@Override
	public int getPdfPasswordEditField() {
		return R.id.etPassword;
	}

	@Override
	public int getPdfPasswordExitButton() {
		return R.id.btn_Exit;
	}

	@Override
	public int getPdfPasswordLayoutResource() {
		return R.layout.pdf_file_password;
	}

	@Override
	public int getPdfPasswordOkButton() {
		return R.id.btn_Ok;
	}

	@Override
	public int getPreviousPageImageResource() {
		return R.drawable.left_arrow;
	}

	@Override
	public int getZoomInImageResource() {
		return R.drawable.zoom_in;
	}

	@Override
	public int getZoomOutImageResource() {
		return R.drawable.zoom_out;
	}

}
