package com.olehdatsko.asynctask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import net.sf.andpdf.pdfviewer.PdfViewerActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class MainActivity extends Activity {
	EditText eEditText;
	Button mButton;
	Downloader loader;
	Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		context = getApplicationContext();
		
		loader = new Downloader((ProgressBar)findViewById(R.id.progressBar1), getApplicationContext());
		
		eEditText = (EditText) findViewById(R.id.url_load);
		mButton = (Button) findViewById(R.id.btn_download);
		
		mButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(eEditText.getText().length() > 0)
					loader.execute(eEditText.getText().toString());
			}
		});	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}

class Downloader extends AsyncTask<String, Void, String>{
    
	InputStream input = null;
    FileOutputStream output = null;
	HttpURLConnection connection = null;
	String name = "";
	Context context;
	
	ProgressBar progress;
	
	public Downloader(ProgressBar prgs, Context cont){
		progress = prgs;
		progress.setMax(100);
		
		context = cont;
	}
	
	@Override
	protected String doInBackground(String... params) {

		try {
	        URL url = new URL(params[0]);
	        connection = (HttpURLConnection) url.openConnection();

	        connection.connect();
	
	        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
	            return "Server returned HTTP " + connection.getResponseCode()
	                    + " " + connection.getResponseMessage();
	        }
	
	        int fileLength = connection.getContentLength();
	
	        input = connection.getInputStream();
	        name = Environment.getExternalStorageDirectory().getAbsolutePath() + params[0].substring(params[0].lastIndexOf('/'), params[0].length());
	        
	        new File(name).delete();
	        
	        output = new FileOutputStream(name);
	        
	        byte data[] = new byte[4096];
	        long total = 0;
	        int count;
	        while ((count = input.read(data)) != -1) {
	            if (isCancelled()) {
	                input.close();
	                return null;
	            }
	            total += count;
	            
	            if (fileLength > 0)
	                progress.setProgress((int) (total * 100 / fileLength));
	            output.write(data, 0, count);
	        }
	    }
		catch (Exception e) {
	        return e.toString();
	    }
		finally {
			try {
				if (output != null)
					output.close();
	            if (input != null)
	                input.close();
			}
			
			catch (IOException ignored) {
			}
	
	        if (connection != null)
	            connection.disconnect();
	        return null;
	    }
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		Intent intent = new Intent(context, OpenPDF.class);
		intent.putExtra(PdfViewerActivity.EXTRA_PDFFILENAME, name);
		intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
}
